﻿#include "AlbumManager.h"
#include <iostream>
#include "Constants.h"
#include "MyException.h"
#include "AlbumNotOpenException.h"
#include <comdef.h>
#include <process.h>
#include <Tlhelp32.h>
#include <comdef.h>

using std::string;

const string AlbumManager::PROGRAM_1_NAME = "mspaint.exe";
const string AlbumManager::PROGRAM_2_NAME = "i_view64.exe";
const string AlbumManager::PROGRAM_1_PATH = R"(C:\Windows\System32\)";
const string AlbumManager::PROGRAM_2_PATH = R"(C:\Program Files\IrfanView\)";

AlbumManager::AlbumManager(IDataAccess& dataAccess) :
	m_dataAccess(dataAccess)
{
	// Left empty
	m_dataAccess.open();
}

void AlbumManager::executeCommand(CommandType command) 
{
	try 
	{
		AlbumManager::handler_func_t handler = m_commands.at(command);
		(this->*handler)();
	}
	catch (const std::out_of_range&) 
	{
			throw MyException("Error: Invalid command[" + std::to_string(command) + "]\n");
	}
}

void AlbumManager::printHelp() const
{
	std::cout << "Supported Album commands:" << std::endl;
	std::cout << "*************************" << std::endl;
	
	for (const struct CommandGroup& group : m_prompts) 
	{
		std::cout << group.title << std::endl;
		string space(".  ");
	
		for (const struct CommandPrompt& command : group.commands) 
		{
			space = command.type < 10 ? ".   " : ".  ";

			std::cout << command.type << space << command.prompt << std::endl;
		}
		std::cout << std::endl;
	}
}


// ******************* Album ******************* 
void AlbumManager::createAlbum()
{
	string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) 
		throw MyException("Error: Can't create album since there is no user with id [" + userIdStr+"]\n");
	

	string name = getInputFromConsole("Enter album name - ");
	if ( m_dataAccess.doesAlbumExists(name,userId) ) 
		throw MyException("Error: Failed to create album, album with the same name already exists\n");
	

	Album newAlbum(m_dataAccess.getIdForNewAlbum(), userId,name);
	m_dataAccess.createAlbum(newAlbum);

	std::cout << "Album [" << newAlbum.getName() << "] created successfully by user@" << newAlbum.getOwnerId() << std::endl;
}

void AlbumManager::openAlbum()
{
	if (isCurrentAlbumSet()) 
		closeAlbum();
	

	string userIdStr = getInputFromConsole("Enter user id: ");
	m_openedUserId = std::stoi(userIdStr);
	
	if ( !m_dataAccess.doesUserExists(m_openedUserId) )
		throw MyException("Error: Can't open album since there is no user with id @" + userIdStr + ".\n");
	

	string name = getInputFromConsole("Enter album name - ");
	if ( !m_dataAccess.doesAlbumExists(name, m_openedUserId) )
		throw MyException("Error: Failed to open album, since there is no album with name " + name + ".\n");
	

	m_openAlbum = m_dataAccess.openAlbum(name, m_openedUserId);
    m_currentAlbumName = name;
	// success
	std::cout << "Album [" << name << "] opened successfully." << std::endl;
}

void AlbumManager::closeAlbum()
{
	refreshOpenAlbum();

	m_dataAccess.closeAlbum(m_openAlbum);
	m_currentAlbumName = "";
	m_openedUserId = 0;
	std::cout << "Album [" << m_openAlbum.getName() << "] closed successfully." << std::endl;
}

void AlbumManager::deleteAlbum()
{
	string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId)) 
		throw MyException("Error: There is no user with id @" + userIdStr +"\n");


	string albumName = getInputFromConsole("Enter album name - ");
	if ( !m_dataAccess.doesAlbumExists(albumName, userId) ) 
		throw MyException("Error: Failed to delete album, since there is no album with name:" + albumName + ".\n");
	

	// album exist, close album if it is opened
	if ( (isCurrentAlbumSet() ) &&
		 (m_openAlbum.getOwnerId() == userId && m_openAlbum.getName() == albumName) ) 
	{
		closeAlbum();
	}

	m_dataAccess.deleteAlbum(albumName, userId);
	std::cout << "Album [" << albumName << "] @"<< userId <<" deleted successfully." << std::endl;
}

void AlbumManager::listAlbums()
{
	m_dataAccess.printAlbums();
}

void AlbumManager::listAlbumsOfUser()
{
	string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId)) 
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	

	const User& user = m_dataAccess.getUser(userId);
	const std::list<Album>& albums = m_dataAccess.getAlbumsOfUser(user);

	std::cout << "Albums list of user@" << user.getId() << ":" << std::endl;
	std::cout << "-----------------------" << std::endl;

	for (const auto& album : albums) 
		std::cout <<"   + [" << album.getName() <<"] - created on "<< album.getCreationDate() << std::endl;
	
}


// ******************* Picture ******************* 
void AlbumManager::addPictureToAlbum()
{
	refreshOpenAlbum();

	string picName = getInputFromConsole("Enter picture name: ");
	if (m_openAlbum.doesPictureExists(picName))
		throw MyException("Error: Failed to add picture, picture with the same name already exists.\n");
	

	Picture picture(m_dataAccess.getIdForNewPic(), picName);
	string picPath = getInputFromConsole("Enter picture path: ");
	picture.setPath(picPath);

	m_dataAccess.addPictureToAlbumByName(m_openAlbum.getName(), m_openAlbum.getOwnerId() ,picture);

	std::cout << "Picture [" << picture.getId() << "] successfully added to Album [" << m_openAlbum.getName() << "]." << std::endl;
}

void AlbumManager::removePictureFromAlbum()
{
	refreshOpenAlbum();

	string picName = getInputFromConsole("Enter picture name: ");
	if ( !m_openAlbum.doesPictureExists(picName) ) {
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	}
	
	auto picture = m_openAlbum.getPicture(picName);
	m_dataAccess.removePictureFromAlbumByName(m_openAlbum.getName(), m_openAlbum.getOwnerId() , picture.getName());
	std::cout << "Picture <" << picName << "> successfully removed from Album [" << m_openAlbum.getName() << "]." << std::endl;
}

void AlbumManager::listPicturesInAlbum()
{
	refreshOpenAlbum();

	std::cout << "List of pictures in Album [" << m_openAlbum.getName() 
			  << "] of user@" << m_openAlbum.getOwnerId() <<":" << std::endl;
	
	const std::list<Picture>& albumPictures = m_openAlbum.getPictures();

	for (auto iter = albumPictures.begin(); iter != albumPictures.end(); ++iter)
	{
		std::cout << "   + Picture [" << iter->getId() << "] - " << iter->getName() << 
			"\tLocation: [" << iter->getPath() << "]\tCreation Date: [" <<
				iter->getCreationDate() << "]\tTags: [" << iter->getTagsCount() << "]" << std::endl;
	}
	std::cout << std::endl;
}

void AlbumManager::showPicture()
{
	refreshOpenAlbum();

	string picName = getInputFromConsole("Enter picture name: ");
	if ( !m_openAlbum.doesPictureExists(picName) )
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	
	
	auto pic = m_openAlbum.getPicture(picName);
	if ( !fileExistsOnDisk(pic.getPath()) )
		throw MyException("Error: Can't open <" + picName+ "> since it doesnt exist on disk.\n");
	
	auto option = getInputFromConsole(" 1 to open with " + PROGRAM_1_NAME
									+ "\n else to open with " + PROGRAM_2_NAME
									+ "\n -> ");

	STARTUPINFOA si;
	PROCESS_INFORMATION pi;
	string path = string(option == "1" ? PROGRAM_1_PATH + PROGRAM_1_NAME : 
										 PROGRAM_2_PATH + PROGRAM_2_NAME) 
				  + " " + pic.getPath();
	
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	
	if (!CreateProcessA(NULL, const_cast<char*>(path.c_str()),
						NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
		throw MyException("Error: can't open this program.\n");
	
	
	showingPic = true;

	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

	showingPic = false;
}

void AlbumManager::killPicProccess()
{

	/*snapshot for all running processes*/
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
	PROCESSENTRY32 pEntry;
	/*initializing size - needed for using Process32First*/
	pEntry.dwSize = sizeof(pEntry);
	BOOL hRes = Process32First(hSnapShot, &pEntry);
	/*while first process in pEntry exists*/
	while (hRes)
	{
		/*create const char for string comparison*/
		_bstr_t b(pEntry.szExeFile);
		if (!strcmp(b, PROGRAM_1_NAME.c_str()) ||
			!strcmp(b, PROGRAM_2_NAME.c_str()))
		{
			/*get terminate handle for process, by ID*/
			HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0,
				(DWORD)pEntry.th32ProcessID);
			if (hProcess != NULL)
			{
				/*terminate process*/
				TerminateProcess(hProcess, 9);
				CloseHandle(hProcess);
			}
		}
		/*next process*/
		hRes = Process32Next(hSnapShot, &pEntry);
	}
	CloseHandle(hSnapShot);
}

void AlbumManager::tagUserInPicture()
{
	refreshOpenAlbum();

	string picName = getInputFromConsole("Enter picture name: ");
	if ( !m_openAlbum.doesPictureExists(picName) )
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	
	
	Picture pic = m_openAlbum.getPicture(picName);
	
	string userIdStr = getInputFromConsole("Enter user id to tag: ");
	int userId = std::stoi(userIdStr);
	
	if ( !m_dataAccess.doesUserExists(userId) ) 
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	
	User user = m_dataAccess.getUser(userId);

	m_dataAccess.tagUserInPicture(m_openAlbum.getName(), pic.getName(), m_openAlbum.getOwnerId(), user.getId());
	std::cout << "User @" << userIdStr << " successfully tagged in picture <" << pic.getName() << "> in album [" << m_openAlbum.getName() << "]" << std::endl;
}

void AlbumManager::untagUserInPicture()
{
	refreshOpenAlbum();

	string picName = getInputFromConsole("Enter picture name: ");
	if (!m_openAlbum.doesPictureExists(picName)) 
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	

	Picture pic = m_openAlbum.getPicture(picName);

	string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = stoi(userIdStr);
	if (!m_dataAccess.doesUserExists(userId)) 
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	
	User user = m_dataAccess.getUser(userId);

	if (! pic.isUserTagged(user)) 
		throw MyException("Error: The user was not tagged! \n");
	

	m_dataAccess.untagUserInPicture(m_openAlbum.getName(), pic.getName(), m_openAlbum.getOwnerId() , user.getId());
	std::cout << "User @" << userIdStr << " successfully untagged in picture <" << pic.getName() << "> in album [" << m_openAlbum.getName() << "]" << std::endl;

}

void AlbumManager::listUserTags()
{
	refreshOpenAlbum();

	string picName = getInputFromConsole("Enter picture name: ");
	if ( !m_openAlbum.doesPictureExists(picName) ) 
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	
	auto pic = m_openAlbum.getPicture(picName); 

	const std::set<int> users = pic.getUserTags();
	
	if ( !users.size() ) 
		throw MyException("Error: There is no user tegged in <" + picName + ">.\n");
	

	std::cout << "Tagged users in picture <" << picName << ">:" << std::endl;
	for (const int user_id: users) 
	{
		const User user = m_dataAccess.getUser(user_id);
		std::cout << user << std::endl;
	}
	std::cout << std::endl;

}


// ******************* User ******************* 
void AlbumManager::addUser()
{
	string name = getInputFromConsole("Enter user name: ");

	User user(m_dataAccess.getIdForNewUser(), name);
	
	m_dataAccess.createUser(user);
	std::cout << "User " << name << " with id @" << user.getId() << " created successfully." << std::endl;
}


void AlbumManager::removeUser()
{
	// get user name
	string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);

	if ( !m_dataAccess.doesUserExists(userId) ) 
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");

	
	const User& user = m_dataAccess.getUser(userId);

	if (isCurrentAlbumSet() && userId == m_openAlbum.getOwnerId()) 
		closeAlbum();


	m_dataAccess.deleteUser(user);
	std::cout << "User @" << userId << " deleted successfully." << std::endl;
}

void AlbumManager::listUsers()
{
	m_dataAccess.printUsers();	
}

void AlbumManager::userStatistics()
{
	string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) {
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}

	const User& user = m_dataAccess.getUser(userId);

	std::cout << "user @" << userId << " Statistics:" << std::endl << "--------------------" << std::endl <<
		"  + Count of Albums owned: " << m_dataAccess.countAlbumsOwnedOfUser(user) << std::endl << 
		"  + Count of Albums Tagged: " << m_dataAccess.countAlbumsTaggedOfUser(user) << std::endl <<
		"  + Count of Tags: " << m_dataAccess.countTagsOfUser(user) << std::endl <<
		"  + Avarage Tags per Album: " << m_dataAccess.averageTagsPerAlbumOfUser(user) << std::endl;
}


// ******************* Queries ******************* 
void AlbumManager::topTaggedUser()
{
	const User& user = m_dataAccess.getTopTaggedUser();

	std::cout << "The top tagged user is: " << user.getName() << std::endl;
}

void AlbumManager::topTaggedPicture()
{
	const Picture& picture = m_dataAccess.getTopTaggedPicture();

	std::cout << "The top tagged picture is: " << picture.getName() << std::endl;
}

void AlbumManager::picturesTaggedUser()
{
	string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if ( !m_dataAccess.doesUserExists(userId) ) {
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	}

	auto user = m_dataAccess.getUser(userId);

	auto taggedPictures = m_dataAccess.getTaggedPicturesOfUser(user);

	std::cout << "List of pictures that User@" << user.getId() << " tagged :" << std::endl;
	for (const Picture& picture: taggedPictures) {
		std::cout <<"   + "<< picture << std::endl;
	}
	std::cout << std::endl;
}


// ******************* Help & exit ******************* 
void AlbumManager::exit()
{
	std::exit(EXIT_SUCCESS);
}

BOOL WINAPI AlbumManager::ConsoleHandler(DWORD CEvent)
{
	if (showingPic)
	{
		killPicProccess();
		return TRUE;
	}

	std::cout << "\nGoodbye :))))" << std::endl;
	return FALSE;
}

void AlbumManager::help()
{
	system("CLS");
	printHelp();
}

string AlbumManager::getInputFromConsole(const string& message)
{
	string input;
	do {
		std::cout << message;
		std::getline(std::cin, input);
	} while (input.empty());
	
	return input;
}

bool AlbumManager::fileExistsOnDisk(const string& filename)
{
	struct stat buffer;   
	return (stat(filename.c_str(), &buffer) == 0); 
}

void AlbumManager::refreshOpenAlbum() 
{
	if (!isCurrentAlbumSet())
		throw AlbumNotOpenException();
	
    m_openAlbum = m_dataAccess.openAlbum(m_currentAlbumName, m_openedUserId);
}

bool AlbumManager::isCurrentAlbumSet() const
{
    return !m_currentAlbumName.empty();
}

const std::vector<struct CommandGroup> AlbumManager::m_prompts  = {
	{
		"Supported Albums Operations:\n----------------------------",
		{
			{ CREATE_ALBUM        , "Create album" },
			{ OPEN_ALBUM          , "Open album" },
			{ CLOSE_ALBUM         , "Close album" },
			{ DELETE_ALBUM        , "Delete album" },
			{ LIST_ALBUMS         , "List albums" },
			{ LIST_ALBUMS_OF_USER , "List albums of user" }
		}
	},
	{
		"Supported Album commands (when specific album is open):",
		{
			{ ADD_PICTURE    , "Add picture." },
			{ REMOVE_PICTURE , "Remove picture." },
			{ SHOW_PICTURE   , "Show picture." },
			{ LIST_PICTURES  , "List pictures." },
			{ TAG_USER		 , "Tag user." },
			{ UNTAG_USER	 , "Untag user." },
			{ LIST_TAGS		 , "List tags." }
		}
	},
	{
		"Supported Users commands: ",
		{
			{ ADD_USER         , "Add user." },
			{ REMOVE_USER      , "Remove user." },
			{ LIST_OF_USER     , "List of users." },
			{ USER_STATISTICS  , "User statistics." },
		}
	},
	{
		"Supported Queries:",
		{
			{ TOP_TAGGED_USER      , "Top tagged user." },
			{ TOP_TAGGED_PICTURE   , "Top tagged picture." },
			{ PICTURES_TAGGED_USER , "Pictures tagged user." },
		}
	},
	{
		"Supported Operations:",
		{
			{ HELP , "Help (clean screen)" },
			{ EXIT , "Exit." },
		}
	}
};

const std::map<CommandType, AlbumManager::handler_func_t> AlbumManager::m_commands = {
	{ CREATE_ALBUM, &AlbumManager::createAlbum },
	{ OPEN_ALBUM, &AlbumManager::openAlbum },
	{ CLOSE_ALBUM, &AlbumManager::closeAlbum },
	{ DELETE_ALBUM, &AlbumManager::deleteAlbum },
	{ LIST_ALBUMS, &AlbumManager::listAlbums },
	{ LIST_ALBUMS_OF_USER, &AlbumManager::listAlbumsOfUser },
	{ ADD_PICTURE, &AlbumManager::addPictureToAlbum },
	{ REMOVE_PICTURE, &AlbumManager::removePictureFromAlbum },
	{ LIST_PICTURES, &AlbumManager::listPicturesInAlbum },
	{ SHOW_PICTURE, &AlbumManager::showPicture },
	{ TAG_USER, &AlbumManager::tagUserInPicture, },
	{ UNTAG_USER, &AlbumManager::untagUserInPicture },
	{ LIST_TAGS, &AlbumManager::listUserTags },
	{ ADD_USER, &AlbumManager::addUser },
	{ REMOVE_USER, &AlbumManager::removeUser },
	{ LIST_OF_USER, &AlbumManager::listUsers },
	{ USER_STATISTICS, &AlbumManager::userStatistics },
	{ TOP_TAGGED_USER, &AlbumManager::topTaggedUser },
	{ TOP_TAGGED_PICTURE, &AlbumManager::topTaggedPicture },
	{ PICTURES_TAGGED_USER, &AlbumManager::picturesTaggedUser },
	{ HELP, &AlbumManager::help },
	{ EXIT, &AlbumManager::exit }
};
