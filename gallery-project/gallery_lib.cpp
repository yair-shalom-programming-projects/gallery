#pragma warning(disable : 4996)
#include "gallery_lib.h"
#include <sstream>
#include <iomanip>
#include <ctime>
#include "SqlException.h"
#include "Album.h"
#include <list>

using std::string;
using std::cout;
using std::endl;
using std::list;

/*
	-- get current time & date --
	* input: string container to contain the time
*/
 void gallery_lib::getCurrentTime(std::string& container)
{
	time_t now = time(nullptr);

	std::stringstream oss;
	oss << std::put_time(localtime(&now), "%d/%m/%Y %H:%M:%S");
	
	container = oss.str();
}
 
