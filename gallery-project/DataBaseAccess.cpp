#include "DataBaseAccess.h"
#include "SqlException.h"
#include "ItemNotFoundException.h"
#include "gallery_lib.h"
#include <sstream>
#include "iostream"
#include "io.h"
#include <iomanip>

using std::stringstream;
using std::string;
using std::list;
using std::cout;
using std::endl;


//? Methods ?//

/* -- c'tor -- */
DataBaseAccess::DataBaseAccess() :
	_db(nullptr)
{
	// if not exists - create new database
	bool notExists = _access(DB_FILE_NAME, 0);

	if (!open())
		throw SqlException("Can't connect to sql DB.");
	
	if (notExists) 
		createDB();
}


/*
	-- open/connect to data base --
	* output: bool - opened or not
*/
bool DataBaseAccess::open()
{
	return sqlite3_open(DB_FILE_NAME, &_db) == SQLITE_OK;
}


/* -- close connection with sql server -- */
void DataBaseAccess::close()
{
	sqlite3_close(_db);
	_db = nullptr;
}


void DataBaseAccess::clear()
{
	// nothing to do here
}


//? ALBUM RELATED METHODS ?//


/*
	-- close album --
	* input: album to close
*/
void DataBaseAccess::closeAlbum(Album&)
{
	// nothing to do here
}


/* -- print all albums -- */
void DataBaseAccess::printAlbums()
{
	stringstream sql;

	auto albums = getAlbums();

	cout << "Album list:" << endl;
	cout << "-----------" << endl;
	
	for (auto album : albums)
		cout << std::setw(5) << "* " << album;
}


/*
	-- get all albums --
	* output: albums container list
*/
const list<Album> DataBaseAccess::getAlbums()
{
	list<Album> albums;
	stringstream sql;

	sql << "SELECT * FROM " << ALBUMS_TABLE << ";";

	sqlRequest(sql, getAlbumsFromSql, &albums);
	
	return albums;
}


/*
	-- delete albums by user's id --
	* input: user's id, bool - start transaction or not		
*/
void DataBaseAccess::deleteAlbumsByUser(const int& userId, const bool transaction)
{
	stringstream sql;
	User user(userId, "");

	try
	{
		if (transaction)
			sqlRequest("BEGIN;");
	
		list<Album> usersAlbums = getAlbumsOfUser(user);
		sql << "DELETE FROM " << ALBUMS_TABLE << " WHERE "
			<< USER_ID_COLUMN << " = " << userId << ";";

		sqlRequest(sql);

		// remove all pictures in all user's albums
		for (auto& album : usersAlbums)
			removePicsByAlbum(album.getId(), false);

		if (transaction)
			sqlRequest("END;");
	}
	catch (const SqlException& e)
	{
		if (transaction)
			sqlRequest("END;");
		throw e;
	}
}


/*
	-- get all albums owned by user --
	* input: user
	* output: list of albums owned by user
*/
const list<Album> DataBaseAccess::getAlbumsOfUser(const User& user)
{
	list<Album> albums;
	stringstream sql;

	sql << "SELECT * FROM " << ALBUMS_TABLE << " WHERE "
		<< USER_ID_COLUMN << " = " << user.getId() << ";";

	sqlRequest(sql, getAlbumsFromSql, &albums);
	return albums;
}


/*
	-- create album --
	* input: album
*/
void DataBaseAccess::createAlbum(const Album& album)
{
	stringstream sql;
	sql << "INSERT INTO " << ALBUMS_TABLE
		<< " ( "
		<< ID_COLUMN << ", " << NAME_COLUMN << ", " 
		<< CREATION_DATE_COLUMN << ", " << USER_ID_COLUMN
		<< " ) VALUES ( "
		<< album.getId() << ", '" << album.getName() << "', '" 
		<< album.getCreationDate() << "', " << album.getOwnerId()
		<< " );";
	
	sqlRequest(sql);
}

/*
	-- delete album by his giving name and user id --
	* input: album's name, user's id (owner of album)
*/
void DataBaseAccess::deleteAlbum(const string& albumName, const int& userId)
{

	stringstream sql;
	
	try
	{
		sqlRequest("BEGIN;");
		
		// sql command
		sql << "DELETE FROM " << ALBUMS_TABLE << " WHERE "
			<< NAME_COLUMN << " = '" << albumName << "' AND "
			<< USER_ID_COLUMN << " = " << userId << ";";

		// execute sql command, if fail throw SqlException
		sqlRequest(sql);

		// remove all pictures in albums
		Album album = getAlbumByName(userId, albumName);
		removePicsByAlbum(album.getId());
		
		sqlRequest("END;");
	}
	catch (const SqlException& e)
	{
		sqlRequest("END;");
		throw e;
	}
}


/*
	-- find if given album exists --
	* input: album's name, user's id
	* output: bool - exists or not
*/
bool DataBaseAccess::doesAlbumExists(const string& albumName, const int& userId)
{
	stringstream sql;
	int exists;

	sql << "SELECT COUNT() FROM " << ALBUMS_TABLE << " WHERE "
		<< NAME_COLUMN << " = '" << albumName << "' AND "
		<< USER_ID_COLUMN << " = " << userId << ";";

	sqlRequest(sql, getNumFromSql, &exists);

	return exists;
}


/*
	-- open album by the given album's name and user id --
	* input: album's name, user's id
	* output: opened album
*/
Album DataBaseAccess::openAlbum(const string& albumName, const int& userId)
{
	return getAlbumByName(userId, albumName);
}




//? PICTURES RELATED METHODS ?//


/*
	-- get all pictures by album's id --
	* input: list to contain pictures, album's id
*/
void DataBaseAccess::getPicsByAlbum(list<Picture>& pics, const int& albumId)
{
	stringstream sql;

	sql << "SELECT * FROM " << PICS_TABLE << " WHERE "
		<< ALBUM_ID_COLUMN << " = " << albumId << ";";
	
	sqlRequest(sql, getPicturesFromSql, &pics);
}


/*
	-- remove all pictures by album's id --
	* input: album's id, bool - start transaction or not
*/
void DataBaseAccess::removePicsByAlbum(const int& albumId, const bool transaction)
{
	stringstream sql;

	list<Picture> pics;

	try
	{
		if (transaction)
			sqlRequest("BEGIN;");

		getPicsByAlbum(pics, albumId);
		sql << "DELETE FROM " << PICS_TABLE << " WHERE "
			<< ALBUM_ID_COLUMN << " = " << albumId << ";";

		sqlRequest(sql);

		// untag all tags in album's pics
		for (auto& pic : pics)
			untagTagsByPic(pic.getId());

		if (transaction)
			sqlRequest("END;");

	}
	catch (const SqlException& e)
	{
		if (transaction)
			sqlRequest("END;");
		throw e;
	}
}


/*
	-- get all tags by pic's id --
	* input: tags container, pic's id
*/
auto DataBaseAccess::getTagsByPic(const int& picId)
{
	std::set<int> tags;
	stringstream sql;

	sql << "SELECT * FROM " << TAGS_TABLE << " WHERE "
		<< PICTURE_ID_COLUMN << " = " << picId << ";";

	sqlRequest(sql, getTagsFromSql, &tags);

	return tags;
}


/*
	-- remove all tags from pictures --
	* input: picture's id
*/
void DataBaseAccess::untagTagsByPic(const int& picId)
{
	stringstream sql;

	sql << "DELETE FROM " << TAGS_TABLE << " WHERE "
		<< PICTURE_ID_COLUMN << " = " << picId << ";";

	sqlRequest(sql);
}


/*
	-- add picture to a given album's name --
	* input: album's name, user's id, picture to add
*/
void DataBaseAccess::addPictureToAlbumByName(const string& albumName, const int& userId, const Picture& picture)
{

	stringstream sql;

	try
	{
		// start transaction
		sqlRequest("BEGIN;");

		// get album
		Album album = getAlbumByName(userId, albumName);

	
		sql << "INSERT INTO " << PICS_TABLE 
			<< " ( " << ID_COLUMN << ", " << NAME_COLUMN << ", " 
			<< LOCATION_COLUMN << ", " << CREATION_DATE_COLUMN << ", " 
			<< ALBUM_ID_COLUMN << " ) VALUES ( " 
			<< picture.getId() << ", '"
			<< picture.getName() << "', '"
			<< picture.getPath() << "', '"
			<< picture.getCreationDate() << "', "
			<< album.getId()
			<< " );";

		sqlRequest(sql);
		sqlRequest("END;");
	}
	catch (const SqlException& e)
	{
		sqlRequest("END;");
		throw e;
	}

}


/*
	-- remove picture by given album's name --
	* input: album's name, picture's name
*/
void DataBaseAccess::removePictureFromAlbumByName(const string& albumName, 
	const int& userId, const string& pictureName)
{

	stringstream sql;

	try
	{
		// start transaction
		sqlRequest("BEGIN;");

		// get album
		Album album = getAlbumByName(userId, albumName);


		sql << "DELETE FROM " << PICS_TABLE << " WHERE " 
			<< ALBUM_ID_COLUMN << " = " << album.getId() << " AND " 
			<< NAME_COLUMN << " = '" << pictureName << "';";

		sqlRequest(sql);
		
		// untag all tags in picture
		untagTagsByPic(getPicByName(album.getId(), pictureName).getId());

		sqlRequest("END;");

	}
	catch (const SqlException& e)
	{
		sqlRequest("END;");
		throw e;
	}
}




//? TAGS RELATED METHODS ?//


/*
	-- tag a user in given picture's name from given album's name --
	* input: album's name, picture's name, owner's id, user's id
*/
void DataBaseAccess::tagUserInPicture(const string& albumName,
	const string& pictureName, const int& ownerId, const int& userId)
{

	stringstream sql;

	try
	{

		// start transaction
		sqlRequest("BEGIN;");

		Album album = getAlbumByName(ownerId, albumName);
		Picture pic = getPicByName(album.getId(), pictureName);


		sql << "INSERT INTO " << TAGS_TABLE
			<< " ( " << PICTURE_ID_COLUMN << ", " << USER_ID_COLUMN 
			<< " ) VALUES ( "
			<< pic.getId() << ", " << userId
			<< " );";

		sqlRequest(sql);
		sqlRequest("END;");

	}
	catch (const SqlException& e)
	{
		sqlRequest("END;");
		throw e;
	}
}


/*
	-- untag user in a given picture's name from a given album's name --
	* input: album's name, picture's name, owner's id, user's id
*/
void DataBaseAccess::untagUserInPicture(const string& albumName,
	const string& pictureName, const int& ownerId, const int& userId)
{
	
	stringstream sql;

	try
	{
		// start transaction
		sqlRequest("BEGIN;");

		Album album = getAlbumByName(ownerId, albumName);
		Picture pic = getPicByName(album.getId(), pictureName);

		sql << "DELETE FROM " << TAGS_TABLE
			<< " WHERE " << PICTURE_ID_COLUMN << " = " << pic.getId()
			<< " AND "
			<< USER_ID_COLUMN << " = " << userId << ";";

		sqlRequest(sql);
		sqlRequest("END;");
	}
	catch (const SqlException& e)
	{
		sqlRequest("END;");
		throw e;
	}

}


/* -- print all users -- */
void DataBaseAccess::printUsers()
{

	list<User> users;
	stringstream sql;
	
	// make sql request for all users
	sql << "SELECT * FROM " << USERS_TABLE << ";";

	sqlRequest(sql, DataBaseAccess::getUsersFromSql, &users);


	// print users
	cout << "Users list:" << endl;
	cout << "-----------" << endl;

	for (auto& user : users)
		cout << user << endl;

}


/*
	-- get user by given id --
	* input: user's id
	* output: user
*/
User DataBaseAccess::getUser(const int& id)
{
	list<User> users;
	stringstream sql;

	sql << "SELECT * FROM " << USERS_TABLE << " WHERE "
		<< ID_COLUMN << " = " << id << ";";
	
	sqlRequest(sql, getUsersFromSql, &users);

	// if there's no user with given id, throw exception 
	if (users.empty())
		throw ItemNotFoundException("User", id);
	
	User user = users.front();
	return user;
}

/*
	-- create a user --
	* input: user to create		
*/
void DataBaseAccess::createUser(User& user)
{
	stringstream sql;
	
	sql << "INSERT INTO " << USERS_TABLE
		<< " ( " << ID_COLUMN << ", " << NAME_COLUMN << " ) VALUES ( "
		<< user.getId() << ", '" << user.getName() << "' );";

	// execute sql command
	sqlRequest(sql);
	
}

/* 
	-- delete given user --
	* input: user to deletes
*/
void DataBaseAccess::deleteUser(const User& user)
{
	stringstream sql;

	try
	{
		sqlRequest("BEGIN;");

		sql << "DELETE FROM " << USERS_TABLE << " WHERE " << ID_COLUMN << " = "
			<< user.getId() << ";";

		// execute sql command
		sqlRequest(sql);
		
		// delete user 
		deleteAlbumsByUser(user.getId(), false);
		
		sqlRequest("END;");

	}
	catch (const SqlException& e)
	{
		sqlRequest("END;");
		throw e;
	}
}


/*
	-- find if there is a user with the given id --
	* input: user's id
	* output: bool - exists or not
*/
bool DataBaseAccess::doesUserExists(const int& id)
{
	stringstream sql;
	int exists;

	sql << "SELECT COUNT() FROM " << USERS_TABLE << " WHERE "
		<< ID_COLUMN << " = " << id << ";";

	sqlRequest(sql, getNumFromSql, &exists);

	return exists;
}


/*
	-- get album by given name and user id --
	* input: user's id, album's name
	* output: album
*/
Album DataBaseAccess::getAlbumByName(const int& userId, const std::string& name)
{

	stringstream sql;
	list<Album> albums;
	list <Picture> pics;
	Album album;


	sql << "SELECT * FROM " << ALBUMS_TABLE << " WHERE "
		<< NAME_COLUMN << " = '" << name << "' AND "
		<< USER_ID_COLUMN << " = " << userId << ";";

	sqlRequest(sql, getAlbumsFromSql, &albums);


	// if there's no album with the given name, throw exception
	if (albums.empty())
		throw MyException("No album with name " + name + " exists");
	album = albums.front();

	// get pictures of album
	getPicsByAlbum(pics, album.getId());

	
	// get all tags to each pic
	for (auto& pic : pics)
		pic.setTags(getTagsByPic(pic.getId()));

	album.setPictures(pics);

	return album;

}


/*
	-- get picture by given name and album id --
	* input: album's id, pic's name
	* output: picture
*/
Picture DataBaseAccess::getPicByName(const int& albumId, const std::string& name)
{
	list<Picture> pics;
	stringstream sql;


	// sql command to get album with the given name and user_id
	sql << "SELECT * FROM " << PICS_TABLE << " WHERE " << NAME_COLUMN << " = '" << name
		<< "' AND " << ALBUM_ID_COLUMN << " = " << albumId << ";";


	// get picture from sql server
	sqlRequest(sql, getPicturesFromSql, &pics);

	// if picture isn't exists - throw exception
	if (pics.empty())
		throw ItemNotFoundException("Picture", name);

	Picture pic = pics.front();

	return pic;
}


/*
	-- count the number of albums owned by the given user --
	* input: user
	* output: the number of albums owned by the given user
*/
int DataBaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	stringstream sql;
	int count = 0;

	sql << "SELECT COUNT() FROM " << ALBUMS_TABLE << " JOIN " << USERS_TABLE
		<< " ON " << ALBUMS_TABLE << "." << USER_ID_COLUMN << " = " 
				  << USERS_TABLE << "." << ID_COLUMN << ";";

	sqlRequest(sql, getNumFromSql, &count);

	return count;
}


/*
	-- count the number of albums the user tagged in --
	* input: user
	* output: num of albums the user tagged in
*/
int DataBaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	stringstream sql;
	int count = 0;

	sql << "SELECT COUNT() FROM (SELECT * FROM " << ALBUMS_TABLE << " JOIN " << PICS_TABLE 
		<< " ON " << ALBUMS_TABLE << "." << ID_COLUMN << " = " 
				  << PICS_TABLE << "." << ALBUM_ID_COLUMN << " JOIN " << TAGS_TABLE
		<< " ON " << PICS_TABLE << "." << ID_COLUMN << " = " 
				  << TAGS_TABLE << "." << PICTURE_ID_COLUMN << " WHERE "
		<< ALBUMS_TABLE << "." << USER_ID_COLUMN << " = "
		<< TAGS_TABLE << "." << USER_ID_COLUMN << " GROUP BY " 
		<< ALBUMS_TABLE << "." << NAME_COLUMN << ");";
	
	sqlRequest(sql, getNumFromSql, &count);

	return count;
}


/*
	-- count the tags of user --
	* input: user
	* output: the count of tags of user
*/
int DataBaseAccess::countTagsOfUser(const User& user)
{
	stringstream sql;
	int count = 0;

	sql << "SELECT COUNT() FROM " << USERS_TABLE << " JOIN " << TAGS_TABLE
		<< " ON " << USERS_TABLE << "." << ID_COLUMN << " = "
				  << TAGS_TABLE << "." << USER_ID_COLUMN << " WHERE " 
		<< USER_ID_COLUMN << " = " << user.getId() << ";";

	sqlRequest(sql, getNumFromSql, &count);

	return count;
}


/*
	-- get the average tags of user per album --
	* input: user
	* output: avg tags of user per album
*/
float DataBaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int albumsTagsOfUser = countAlbumsTaggedOfUser(user);

	if (albumsTagsOfUser)
		return static_cast<float>(countTagsOfUser(user) / albumsTagsOfUser);
	
	return 0;
}


/*
	-- get the top tagged user --
*/
User DataBaseAccess::getTopTaggedUser()
{
	list<User> users; 
	stringstream sql;
	

	// get top tagged user
	sql << "SELECT " << USERS_TABLE << ".* FROM " << USERS_TABLE << " JOIN " << TAGS_TABLE
		<< " ON " << USERS_TABLE << "." << ID_COLUMN << " = " << TAGS_TABLE << "." << USER_ID_COLUMN
		<< " GROUP BY " << USERS_TABLE << "." << ID_COLUMN 
		<< " ORDER BY COUNT(" << TAGS_TABLE << "." << ID_COLUMN
		<< ") DESC LIMIT 1;";
	
	sqlRequest(sql, getUsersFromSql, &users);


	// if there isn't any tagged user, throw exception
	if (users.empty())
		throw MyException("There isn't any tagged user.");
	
	User topTagged = users.front();
	
	return topTagged;
}


/*
	-- get the top tagged picture --
	* output: top tagged picture
*/
Picture DataBaseAccess::getTopTaggedPicture()
{
	stringstream sql;
	list<Picture> pics;
	
	sql << "SELECT " << PICS_TABLE << ".* FROM "  << PICS_TABLE << " JOIN " << TAGS_TABLE << " ON " 
		<< PICS_TABLE << "." << ID_COLUMN << " = " << TAGS_TABLE << "." << PICTURE_ID_COLUMN
		<< " GROUP BY " << PICS_TABLE << "." << ID_COLUMN
		<< " ORDER BY COUNT(" << TAGS_TABLE << "." << ID_COLUMN
		<< ") DESC LIMIT 1;";

	sqlRequest(sql, getPicturesFromSql, &pics);
	
	if (pics.empty())
		throw MyException("There isn't any tagged picture.");

	Picture pic = pics.front();
	return pic;
}


/*
	-- get all the pictures the user is tagged in --
	* input: user
	* output: list of pictures the user is tagged in
*/
list<Picture> DataBaseAccess::getTaggedPicturesOfUser(const User& user)
{
	list<Picture> pics;
	std::set<int> tags;
	stringstream sql;

	sql << "SELECT " << PICS_TABLE << ".* FROM " << PICS_TABLE << " JOIN " << TAGS_TABLE
		<< " ON " << PICS_TABLE << "." << ID_COLUMN << " = " 
				  << TAGS_TABLE << "." << PICTURE_ID_COLUMN 
		<< " WHERE " << TAGS_TABLE << "." << USER_ID_COLUMN << " = " << user.getId()
		<< " GROUP BY " << PICTURE_ID_COLUMN << ";";
	sqlRequest(sql, getPicturesFromSql, &pics);
	

	// get tags to each pic
	for (auto& pic : pics)
		pic.setTags(getTagsByPic(pic.getId()));
	
	return pics;
}



/*
   -- execute sql request --
   * input: sql command stringstream, callback function,
			data to pass to callback function
*/
void DataBaseAccess::sqlRequest(const std::stringstream& sql,
	int(*callback)(void*, int, char**, char**), void* data)
{
	sqlRequest(sql.str().c_str(), callback, data);
}


/*
  -- execute sql request --
  * input: sql command, callback function,
   data to pass to callback function
*/
void DataBaseAccess::sqlRequest(const char* sql,
	int(*callback)(void*, int, char**, char**), void* data)
{
	char* errMsg = nullptr;
	if (sqlite3_exec(_db, sql, callback, data, &errMsg) != SQLITE_OK)
		throw SqlException(errMsg);
}



/* -- create DataBase Tables -- */
void DataBaseAccess::createDB()
{
	stringstream sql;

	//albums
	sql << "CREATE TABLE " << ALBUMS_TABLE << "(" 
		<< ID_COLUMN << " INTEGER PRIMARY KEY NOT NULL, " 
		<< NAME_COLUMN << " TEXT NOT NULL, " 
		<< CREATION_DATE_COLUMN << " DATE NOT NULL, "
		<< USER_ID_COLUMN << " INTEGER NOT NULL, "
		<< "FOREIGN KEY (" << USER_ID_COLUMN << ") REFERENCES " 
		<< USERS_TABLE << "(" << ID_COLUMN 
		<< "));";

	sqlRequest(sql);
	
	sql.str("");

	// pictures
	sql << "CREATE TABLE " << PICS_TABLE << "("
		<< ID_COLUMN << " INTEGER PRIMARY KEY NOT NULL, "
		<< NAME_COLUMN << " TEXT NOT NULL, "
		<< LOCATION_COLUMN << " TEXT NOT NULL, "
		<< CREATION_DATE_COLUMN << " DATE NOT NULL, "
		<< ALBUM_ID_COLUMN << " INTEGER NOT NULL, "
		<< "FOREIGN KEY (" << ALBUM_ID_COLUMN << ") REFERENCES "
		<< ALBUMS_TABLE << "(" << ID_COLUMN
		<< "));";
	sqlRequest(sql);
	
	sql.str("");

	// users
	sql << "CREATE TABLE " << USERS_TABLE << "("
		<< ID_COLUMN << " INTEGER PRIMARY KEY NOT NULL, "
		<< NAME_COLUMN << " TEXT NOT NULL" << ");";
	sqlRequest(sql);
	
	sql.str("");

	//tags
	sql << "CREATE TABLE " << TAGS_TABLE << "("
		<< ID_COLUMN << " INTEGER PRIMARY KEY NOT NULL, "
		<< PICTURE_ID_COLUMN << " INTEGER NOT NULL, "
		<< USER_ID_COLUMN << " INTEGER NOT NULL, "
		<< "FOREIGN KEY (" << PICTURE_ID_COLUMN << ") REFERENCES "
		<< PICS_TABLE << "(" << ID_COLUMN << "), "
		<< "FOREIGN KEY (" << USER_ID_COLUMN << ") REFERENCES "
		<< USERS_TABLE << "(" << ID_COLUMN
		<< "));";
	sqlRequest(sql);
}




//? SQL CALLBACK ?// 


int DataBaseAccess::getAlbumsFromSql(void* voidAlbum, int len, char** data, char** columnName)
{

	auto albums = static_cast<list<Album>*>(voidAlbum);
	Album album;


	// get albums data
	for (int i = 0; i < len; ++i)
	{
		if (string(columnName[i]) == ID_COLUMN)
		{
			album.setId(atoi(data[i]));
		}
		else if (string(columnName[i]) == NAME_COLUMN)
		{
			album.setName(data[i]);
		}
		else if (string(columnName[i]) == CREATION_DATE_COLUMN)
		{
			album.setCreationDate(data[i]);
		}
		else if (string(columnName[i]) == USER_ID_COLUMN)
		{
			album.setOwner(atoi(data[i]));
		}
	}
	
	albums->push_back(album);

	return 0;

}
int DataBaseAccess::getPicturesFromSql(void* voidPic, int len, char** data, char** columnName)
{

	auto pics = static_cast<list<Picture>*>(voidPic);

	Picture pic;

	// get album data
	for (int i = 0; i < len; ++i)
	{
		if (string(columnName[i]) == ID_COLUMN)
		{
			pic.setId(atoi(data[i]));
		}
		else if (string(columnName[i]) == NAME_COLUMN)
		{
			pic.setName(data[i]);
		}
		else if (string(columnName[i]) == LOCATION_COLUMN)
		{
			pic.setPath(data[i]);
		}
		else if (string(columnName[i]) == CREATION_DATE_COLUMN)
		{
			pic.setCreationDate(data[i]);
		}
	}
	
	pics->push_back(pic);

	return 0;
}
int DataBaseAccess::getUsersFromSql(void* voidPic, int len, char** data, char** columnName)
{

	User user;

	// get album data
	for (int i = 0; i < len; ++i)
	{
		if (string(columnName[i]) == ID_COLUMN)
		{
			user.setId(atoi(data[i]));
		}
		else if (string(columnName[i]) == NAME_COLUMN)
		{
			user.setName(data[i]);
		}
	}

	auto users = static_cast<list<User>*>(voidPic);
	users->push_back(user);
	return 0;
}
int DataBaseAccess::getNumFromSql(void* num, int len, char** data, char** columnName)
{
	int* n = static_cast<int*>(num);

	*n = data[0] ? atoi(data[0]) : 0;
	
	return 0;
}
int DataBaseAccess::getTagsFromSql(void* voidTags, int len, char** data, char** columnName)
{
	auto tags = static_cast<std::set<int>*>(voidTags);

	for (int i = 0; i < len; ++i)
	{
		if (string(columnName[i]) == USER_ID_COLUMN)
		{
			tags->insert(atoi(data[i]));
			break;
		}
	}
	return 0;
}




//? GENERATE NEXT ID ?//


/*
	-- generate new id for new table -- 
	* output: new id for the new table
*/
int DataBaseAccess::generateNewId(const char* tableName)
{
	stringstream sql;
	int biggest = 0;

	sql << "SELECT MAX(" << ID_COLUMN << ") FROM " << tableName;

	sqlRequest(sql, getNumFromSql, &biggest);
	
	return biggest + 1;
}
int DataBaseAccess::getIdForNewAlbum()
{
	return generateNewId(ALBUMS_TABLE);
}
int DataBaseAccess::getIdForNewUser()
{
	return generateNewId(USERS_TABLE);
}
int DataBaseAccess::getIdForNewPic()
{
	return generateNewId(PICS_TABLE);
}
int DataBaseAccess::getIdForNewTag()
{
	return generateNewId(TAGS_TABLE);
}