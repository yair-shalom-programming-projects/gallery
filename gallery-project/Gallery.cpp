#include <iostream>
#include <string>
#include "DataBaseAccess.h"
#include "AlbumManager.h"
#include "gallery_lib.h"
#include <Windows.h>

using std::string;
using std::endl;
using std::cout;

bool AlbumManager::showingPic = false;


int getCommandNumberFromUser()
{
	string message("\nPlease enter any command(use number): ");
	string numericStr("0123456789");
			
	cout << message << endl;
	string input;
	std::getline(std::cin, input);
	int i = input.size();

	while (std::cin.fail() || std::cin.eof() || input.find_first_not_of(numericStr) != string::npos) 
	{

		cout << "Please enter a number only!" << endl;

		if (input.find_first_not_of(numericStr) == string::npos)
			std::cin.clear();
		

		cout << endl << message << endl;
		std::getline(std::cin, input);
	}
	
	return std::atoi(input.c_str());
}

int main(void)
{
	// initialization data access
	DataBaseAccess dataAccess;
	// initialize album manager
	AlbumManager albumManager(dataAccess);
	
	string albumName;
	string time;


	if (!SetConsoleCtrlHandler(AlbumManager::ConsoleHandler, TRUE))
	{
		cout << "Error while starting Gallery. Please try again" << endl;
		return -1;
	}

	gallery_lib::getCurrentTime(time);
	
	cout << "\tdeveloped by Yair Shalom | " << time << endl;
	cout << "\t----------------------------------------------" << endl;
	cout << "\nWelcome to Gallery!" << endl;
	cout << "===================" << endl;
	cout << "Type " << HELP << " to a list of all supported commands" << endl;	

	do 
	{
		int commandNumber = getCommandNumberFromUser();
		
		try	
		{
			albumManager.executeCommand(static_cast<CommandType>(commandNumber));
		}
		catch (std::exception& e) 
		{	
			cout << e.what() << endl;
		}
	} 
	while (true);
}


