#pragma once
#include "Album.h"
#include "IDataAccess.h"
#include "sqlite3.h"


class DataBaseAccess : public IDataAccess
{
public: 
	// constructors
	DataBaseAccess();
	virtual ~DataBaseAccess() = default;


	// methods
	virtual bool open() override;
	virtual void close() override;
	virtual void clear() override;

	// album related
	virtual const std::list<Album> getAlbums() override;
	void deleteAlbumsByUser(const int& userId, const bool transaction = true);
	virtual const std::list<Album> getAlbumsOfUser(const User& user) override;
	
	virtual void createAlbum(const Album& album) override;
	virtual void deleteAlbum(const std::string& albumName, const int& userId) override;
	
	virtual bool doesAlbumExists(const std::string& albumName, const int& userId) override;
	
	virtual Album openAlbum(const std::string& albumName, const int& userId) override;
	virtual void closeAlbum(Album& pAlbum) override;
	
	virtual void printAlbums() override;
	

	// pictures related
	void getPicsByAlbum(std::list<Picture>& pics, const int& albumId);
	void removePicsByAlbum(const int& albumId, const bool transaction = true);
	
	virtual void addPictureToAlbumByName(const std::string& albumName, const int& userId, const Picture& picture) override;
	virtual void removePictureFromAlbumByName(const std::string& albumName, const int& userId, const std::string& pictureName) override;
	
	// tags related
	auto getTagsByPic(const int& picId);
	void untagTagsByPic(const int& picId);

	virtual void tagUserInPicture(const std::string& albumName, const std::string& pictureName, const int& ownerId, const int& userId) override;
	virtual void untagUserInPicture(const std::string& albumName, const std::string& pictureName, const int& ownerId, const int& userId) override;

	// users related
	virtual User getUser(const int& userId) override;
	
	virtual bool doesUserExists(const int& userId) override;
	
	virtual void createUser(User& user) override;
	virtual void deleteUser(const User& user) override;
	
	virtual void printUsers() override;


	// user statistics
	virtual int countAlbumsOwnedOfUser(const User& user) override;
	virtual int countAlbumsTaggedOfUser(const User& user) override;
	virtual int countTagsOfUser(const User& user) override;
	virtual float averageTagsPerAlbumOfUser(const User& user) override;

	// queries
	virtual User getTopTaggedUser() override;
	virtual Picture getTopTaggedPicture() override;
	virtual std::list<Picture> getTaggedPicturesOfUser(const User& user) override;
	
	// sql callback
	static int getAlbumsFromSql(void* voidAlbum, int len, char** data, char** columnName);
	static int getPicturesFromSql(void* voidPic, int len, char** data, char** columnName);
	static int getUsersFromSql(void* voidPic, int len, char** data, char** columnName);
	static int getNumFromSql(void* num, int len, char** data, char** columnName);
	static int getTagsFromSql(void* num, int len, char** data, char** columnName);
	
	// generate id 
	virtual int getIdForNewAlbum() override;
	virtual int getIdForNewUser() override;
	virtual int getIdForNewPic() override;
	virtual int getIdForNewTag() override;
	int generateNewId(const char* tableName);

private:
	void createDB();
	
	void sqlRequest(const std::stringstream& sql,
		int(*callback)(void*, int, char**, char**) = nullptr, void* data = nullptr);
	void sqlRequest(const char* sql,
		int(*callback)(void*, int, char**, char**) = nullptr, void* data = nullptr);


	Album getAlbumByName(const int& userId, const std::string& name);
	Picture getPicByName(const int& albumId, const std::string& name);


	const char* DB_FILE_NAME = "DB.sqlite";

	// tables names
	static constexpr const char* ALBUMS_TABLE = "Albums";
	static constexpr const char* PICS_TABLE = "Pictures";
	static constexpr const char* USERS_TABLE = "Users";
	static constexpr const char* TAGS_TABLE = "Tags";

	// columns names
	static constexpr const char* NAME_COLUMN = "name";
	static constexpr const char* ID_COLUMN = "id";
	static constexpr const char* LOCATION_COLUMN = "location";
	static constexpr const char* CREATION_DATE_COLUMN = "Creation_date";
	static constexpr const char* PICTURE_ID_COLUMN = "picture_id";
	static constexpr const char* ALBUM_ID_COLUMN = "album_id";
	static constexpr const char* USER_ID_COLUMN = "user_id";


	sqlite3* _db;
};


