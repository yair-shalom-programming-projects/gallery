﻿#pragma once
#include "Picture.h"
#include <list>
#include <memory>


class Album
{
public:
    Album() = default;
	Album(const int& id, const int& ownerId, const std::string& name);
	Album(const int& id, const int& ownerId, const std::string& name, const std::string& creationTime);

	
	// setters
	void setCreationDateNow();
	void setCreationDate(const std::string& creationTime);
	void setName(const std::string& name);
	void setOwner(int userId);
	void setId(const int& id);
	void setPictures(const std::list<Picture> pics);

	// getters
	std::string getCreationDate() const;
	const std::string& getName() const;
	Picture getPicture(const std::string& name) const;
	int getOwnerId() const;
	int getId() const;

	bool doesPictureExists(const std::string& name) const;
	void addPicture(const Picture& picture);
	void removePicture(const std::string& pictureName);

	std::list<Picture> getPictures() const;

	void untagUserInPicture(int userId, const std::string& pictureName);
	void untagUserInAlbum(int userId);

	void tagUserInPicture(int userId, const std::string& pictureName);
	void tagUserInAlbum(int userId);
	
	bool operator==(const Album& other) const;
	friend std::ostream& operator<<(std::ostream& strOut, const Album& album);

private:
	int m_id { -1 };
    int m_ownerId { 0 };
	std::string m_name;
	std::string m_creationDate;
	std::list<Picture> m_pictures;
};
